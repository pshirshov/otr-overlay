#!/bin/bash


if [[ $# != 1 ]]; then
    echo "syntax: $0 version"
    exit 1
fi

VERSION="${1/-M/_pre}"

function copy {
    fname=$(ls net-misc/$1/ | sort -n | grep ebuild | grep 9999)
    echo "net-misc/$1/$fname -> net-misc/$1/$1-$VERSION.ebuild"
    cp net-misc/$1/$fname net-misc/$1/$1-$VERSION.ebuild
}

. net-misc/otr/otr-9999.ebuild 2>&1 > /dev/null
for s in $SERVICES; do
  copy otr-${s}
done
