# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"
DESCRIPTION="Omnitrack analyst service"

OTR_IUSE="fueldelta geozone overspeed parking tripschedule"
inherit otr2-3
