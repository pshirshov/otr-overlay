# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"
DESCRIPTION="Omnitrack tracer service"

OTR_IUSE="fuel tracks"
inherit otr2-3
