# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"
DESCRIPTION="Omnitrack registry service"

OTR_IUSE="arusnavi domainsupport"
inherit otr2-3
