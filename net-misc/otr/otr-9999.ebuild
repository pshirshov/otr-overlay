# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit user

DESCRIPTION=""
HOMEPAGE="http://otrdev.no-hope.org/"
SRC_URI=""
LICENSE="LGPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
RESTRICT="primaryuri"

SERVICES="analyst archive collector informer registry tracer webgui"

DEPS="app-admin/supervisor"
for service in ${SERVICES}; do
  DEPS="${DEPS} otr_services_${service}? ( =net-misc/otr-${service}-${PV} )"
  IUSE="${IUSE} +otr_services_${service}"
  #if [ -z "$(eval echo \${OTR_$(echo ${service} |tr '[:lower:]' '[:upper:]')_MODULES+x})" ]; then
  #  IUSE="${IUSE} otr_services_${service}"
  #else
  #  IUSE="${IUSE} +otr_services_${service}"
  #fi
done

DEPEND="virtual/jdk:1.7
        ${DEPS}
        "
RDEPEND="virtual/jre:1.7
         ${DEPS}
         "

S="${WORKDIR}"

INSTALL_DIR="/opt/otr/"

pkg_setup() {
    enewgroup otr
    enewuser otr -1 /bin/bash ${INSTALL_DIR} otr
}

src_install() {
    insinto "${INSTALL_DIR}"
    doins ${FILESDIR}/supervisord_wrapper_ej.py
    fperms 777 "${INSTALL_DIR}/supervisord_wrapper_ej.py"

    fowners -R otr:otr ${INSTALL_DIR}

    keepdir /var/log/otr
    fowners -R otr:otr /var/log/otr
}
