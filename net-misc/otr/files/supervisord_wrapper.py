#!/usr/bin/env python

import os
import sys
from os.path import join, exists, basename, dirname, abspath
from glob import glob

jvm_args = os.getenv('JVM_ARGS')

script = sys.argv[0]
script_name = basename(script)
repository_path = abspath(dirname(script))
project_name = None
main_class = None
vmargs_filename_cmd_line = None


def print_help():
    help_project_path = join(repository_path, '<project_name>')
    help_app_path = join(help_project_path, 'current')
    help_conf_path = join(help_app_path, 'conf')

    print "Simple wrapper for supervisord to start java projects"
    print
    print "Usage:"
    print "%s project_name main_class" % script_name
    print "%s project_name vmargs_filename main_class" % script_name
    print
    print "(I) Directories description: "
    print "  [repository_path] (%s)" % repository_path
    print "   \\"
    print "    +--[prject_path] %s" % help_project_path
    print "     \\"
    print "      +--[app_path] %s" % help_app_path
    print "       \\"
    print "        +--[conf_path] (%s)" % help_conf_path
    print
    print "(II) CLASSPATH resolving:"
    print "  script assumes that jar files placed ito [app_path] directory."
    print "  all jar files will be loaded to classpath."
    print
    print "(III) JVM options resolving:"
    print "  1. [no vmargs_filename passed]:"
    print "    1.1) look for [conf_path]/[project_name].vmargs"
    print "    2.2) if not exists fallback to [app_path]/[project_name].vmargs"
    print "    2.2) if not exists ignore files loading"
    print "  2. [vmargs_filename passed]:"
    print "    overrides 1.1 to [conf_path]/[vmargs_filename].vmargs"
    print "    the rest is same as III.1"
    print "    if file not exists error will be produced"
    print "  3. [JVM_ARGS environment variable passed]"
    print "    all vmargs files ignored, options loaded from environment"
    print "    variable"
    print
    print "(IV) VMARGS file"
    print "  1. each jvm option sgould be at single line"
    print "  2. each occurrence of %(app_path)s %(conf_path) %(project_path)"
    print "     in file will be replaced to real paths from (I)"
    print

def make_vmargs_path(root_dir, filename):
    return join("/etc/otr", "%s.vmargs" % filename)

def get_args(path):
    ret = []
    if exists(path):
        with open(path) as vmargs_file:
            for line in vmargs_file:
                l = line.strip()
                if not l.startswith("#"):
                    ret.append(l % java_project_dict)
    return ret

if len(sys.argv) == 1:
    print_help()
    sys.exit(0)
elif len(sys.argv) == 3:
    project_name = sys.argv[1]
    main_class = sys.argv[2]
elif len(sys.argv) == 4:
    project_name = sys.argv[1]
    vmargs_filename_cmd_line = sys.argv[2]
    main_class = sys.argv[3]
else:
    print_help()
    raise Exception("Incorrect number of parameters")

project_path = os.path.join(repository_path, project_name)
if not exists(project_path):
    raise Exception(
            "Project '%s' not fount in %s" % (project_name, repository_path))

app_path = os.path.join(project_path)
if not exists(project_path):
    raise Exception(
            "Project '%s' don't 'have current' subfolder" % project_name)

conf_path = os.path.join(app_path, 'conf')

# This dict will be used for compile JVM options
java_project_dict = {
    'app_path': app_path,
    'conf_path': conf_path,
    'project_path': project_path
}

project_conf = '/etc/otr/%s/' % project_name

# adding configs to classpath
classpath = '/etc/otr/%s:' % project_name

# adding logback.xml as a toplevel resource
#logback_path = join(project_conf, 'logback.xml')
#if exists(logback_path):
#    classpath += (logback_path+":")

# adding project libraries
classpath += ':'.join(glob(join(app_path, '*.jar')))

# it's dependencies
classpath += ':' + (':'.join(glob(join(app_path, 'lib', '*.jar'))))

# and modules
if exists(join(app_path, 'modules')):
    classpath += ':' + (':'.join(glob(join(app_path, 'modules', '*', '*.jar'))))
    classpath += ':' + (':'.join(glob(join(app_path, 'modules', '*', 'lib', '*.jar'))))

#print classpath

cmd = ['-cp %s' % classpath]

vmargs_path = make_vmargs_path(conf_path,
                               (vmargs_filename_cmd_line or project_name))

#print vmargs_path

if jvm_args:
    cmd.append(jvm_args % java_project_dict)
else:
    vmargs = get_args(vmargs_path)
    if not vmargs:
        # if vmargs passed from commandline then complain!
        if vmargs_filename_cmd_line:
            raise Exception("%s expected but was not found!" % vmargs_path)
        # ... else fallback to [project_path]/[project_name].vmargs
        vmargs = get_args(make_vmargs_path(project_path, project_name))
    cmd += vmargs

# adding yaml config for legacy apps
if exists(join(project_conf, 'config.yml')):
    cmd.append('-Dapp.configFile=/etc/otr/%s/config.yml' % project_name)

cmd.append(main_class)

#print cmd

cmd = ' '.join(cmd).split()
#print 'java', ' '.join(cmd)

cmd.insert(0, script_name)

os.chdir(app_path)
os.execvp('java', cmd)
