# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"
JAVA_PKG_IUSE="test-jar test"
inherit git-2 java-pkg-2 java-ant-2

DESCRIPTION=""
HOMEPAGE="http://example.org/"
EGIT_REPO_URI="git@bitbucket.org:pshirshov/${PN}.git"
EGIT_HAS_SUBMODULES="true"
SRC_URI=""
LICENSE="LGPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPS=">=dev-java/maven-ant-tasks-2.1.3
      >=dev-db/postgis-1.5.3
      >=net-misc/rabbitmq-server-2.6.1
      app-admin/supervisor
      "
DEPEND="virtual/jdk:1.6
        ${DEPS}
        "
RDEPEND="virtual/jre:1.6
         ${DEPS}
         "

EANT_ANT_TASKS="maven-ant-tasks"
EANT_BUILD_TARGET="release"

INSTALL_DIR="/opt/omnitrack/"
CONF_DIR="/etc/omnitrack/"

src_compile() {
    export M2_USER_HOME="${ROOT}/tmp"
    export M2_LOCAL_REPO="${ROOT}/tmp/repository"

    use test || EANT_EXTRA_ARGS="${EANT_EXTRA_ARGS} -DskipTests=test"
    use test-jar && EANT_EXTRA_ARGS="${EANT_EXTRA_ARGS} -DtestJar=test"
    cd ${WORKDIR}/${P}
    java-pkg-2_src_compile
}

#src_configure() {
#    java-pkg-2_src_configure
#}

pkg_setup() {
    enewgroup omnitrack
    enewuser omnitrack -1 /bin/bash ${INSTALL_DIR} omnitrack
}

src_unpack() {
    git-2_src_unpack
}

src_install() {
    insinto "${INSTALL_DIR}"
    doins ${FILESDIR}/supervisord_wrapper.py
    fperms 777 "${INSTALL_DIR}/supervisord_wrapper.py"

    insinto "${CONF_DIR}"
    doins ${FILESDIR}/omnitrack.conf

    mkdir "target/conf"
    for dir in $(ls target/release/)
    do
        if [[ -d "${dir}/conf" ]]; then
          insinto "${INSTALL_DIR}"
          doins -r "target/release/${dir}"

          cp -R "${dir}/conf/" "target/conf/${dir}"

          insinto "${CONF_DIR}"
          doins -r "target/conf/${dir}"
        fi
    done

    fowners -R omnitrack:omnitrack ${INSTALL_DIR}

    keepdir /var/log/omnitrack
    fowners -R omnitrack:omnitrack /var/log/omnitrack
}
