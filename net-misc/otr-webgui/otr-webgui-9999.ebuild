# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"
DESCRIPTION="Omnitrack web interface"

DISABLE_SUPERVISORD_CONF="true"
inherit otr2-3
