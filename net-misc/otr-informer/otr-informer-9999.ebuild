# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"
DESCRIPTION="Omnitrack informer service"

OTR_IUSE="deliver-email filter-analyst renderer-chunk-templates"
inherit otr2-3
