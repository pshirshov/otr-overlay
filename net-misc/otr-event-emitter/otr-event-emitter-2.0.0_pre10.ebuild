# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"
DESCRIPTION="Omnitrack event emitter service"

OTR_DEPS="
    >=dev-db/postgis-1.5.3
    >=net-misc/rabbitmq-server-2.6.1
    "
inherit otr2-3
