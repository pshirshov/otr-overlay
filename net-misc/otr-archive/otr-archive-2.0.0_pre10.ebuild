# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"
DESCRIPTION="Omnitrack archive service"

OTR_IUSE="postgis sensorvalue-arusnavi"
OTR_DEPS=">=dev-db/postgresql-server-9.1
          >=dev-db/postgis-1.5.3-r1
          >=dev-db/mongodb-2.2.0
          "
inherit otr2-3
