# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"
inherit user

EXPORT_FUNCTIONS src_unpack pkg_setup src_install

REAL_PN="${PN:4}"

# TODO: java <-> ebuild mapping
REAL_PV="${PV/_pre/-M}"

# allows to alter version
PKG_TYPE="${PKG_TYPE-zip}"

if [ "${PV}" = "9999" ]; then
  # credentials
  OTR2_USER="${OTR2_USER-ebuild}"
  OTR2_PASSWD="${OTR2_PASSWD-IVe0iucohh}"

  # last successful build from default branch (via REST)
  PKG_URL="${PKG_URL-http://${OTR2_USER}:${OTR2_PASSWD}@dev.otr2.no-hope.org/teamcity/httpAuth/app/rest/builds/branch:(default:true),buildType:(id:bt4),status:SUCCESS/artifacts/files/dist}"
  PKG_POSTFIX=""

  # old-style last succesfull build
  #PKG_URL="${PKG_URL-http://${OTR2_USER}:${OTR2_PASSWD}@dev.otr2.no-hope.org/teamcity/httpAuth/repository/download/bt4/.lastSuccessful/dist/}"
  #PKG_VERSION="${PKG_VERSION-{build.number}}"
else
  PKG_URL="${PKG_URL-http://dev.otr2.no-hope.org/downloads/distrib/otr2/}"
  PKG_VERSION="${PKG_VERSION-${REAL_PV}}"
fi

PKG_POSTFIX=${PKG_POSTFIX--${PKG_VERSION}}

OTR_URI="${PKG_URL}/${REAL_PN}${PKG_POSTFIX}.${PKG_TYPE}"
SRC_URI="${OTR_URI} ${URI_OPTS}"
if [ "${PV}" = "9999" ]; then
  SRC_URI=""
fi

RESTRICT="primaryuri mirror"

HOMEPAGE="http://otrdev.no-hope.org/"
LICENSE="LGPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"

REAL_IUSE=""
SELECTED=$(eval echo \$OTR_$(echo ${REAL_PN} |tr '[:lower:]' '[:upper:]')_MODULES)
uses="${OTR_IUSE:-}"
for otr_iuse in ${uses}; do
    u="otr_${REAL_PN}_modules_${otr_iuse}"
    REAL_IUSE="${REAL_IUSE} +${u}"

    #if [[ "${uses}" == *"${u}"* ]]; then
    #    REAL_IUSE="${REAL_IUSE} +${u}"
    #else
    #    REAL_IUSE="${REAL_IUSE} ${u}"
    #fi
done

IUSE="${REAL_IUSE}"

DEPS="${OTR_DEPS:-}
     "
DEPEND="virtual/jdk:1.7
        ${DEPS}
        "
RDEPEND="virtual/jre:1.7
         ${DEPS}
         "

INSTALL_DIR="/opt/otr/${REAL_PN}"
CONF_DIR="/etc/otr/${REAL_PN}"
CONF_BASEDIR="/etc/otr"

SUPERVISORD_FILE=\
"[program:${REAL_PN}]\n"\
"user=otr\n"\
"command=/opt/otr/supervisord_wrapper_ej.py ${REAL_PN}\n"\
"autostart=true\n"\
"autorestart=true\n"\
"stdout_logfile=/var/log/otr/${REAL_PN}.stdout.log\n"\
"stderr_logfile=/var/log/otr/${REAL_PN}.stderr.log\n"

S="${WORKDIR}"

otr2-3_src_unpack() {
  if [ "${PV}" = "9999" ]; then
    file=${PN}.${PKG_TYPE}
    wget "$OTR_URI" -O ${DISTDIR}/$file

    if [[ ! "$?" = 0 ]]; then
      eerror
      eerror "Unable to download ${file}."
      eerror "Make sure OTR2_USER and OTR2_PASSWD are valid and defined in make.conf"
      eerror

      die "fetch failed"
    fi

    unpack $file
  else
    default_src_unpack
  fi
}

otr2-3_pkg_setup() {
    enewgroup otr
    enewuser otr -1 /bin/bash /opt/otr otr
}

otr2-3_src_install() {
    insinto "${INSTALL_DIR}"

    # installing project binary
    doins *.[wj]ar

    # ... optional dependencies
    if [ -d "lib" ]; then
        doins -r lib || die
    fi

    # ... selected modules
    if [ -d "modules" ]; then
        insinto "${INSTALL_DIR}/modules"
        for mod in ${IUSE}; do
            flag=${mod/+/}
            module="${flag/otr_${REAL_PN}_modules_/}"
            if use ${flag}; then
                if [[ -d "modules/${module}" ]]; then
                    doins -r "modules/${module}" || die
                #FIXME: this code is for legacy builds support
                elif [[ -d "modules/arus" && "${module}" == "arusnavi" ]]; then
                    doins -r "modules/arus" || die
                elif [[ -d "modules/registry-arus" && "${module}" == "arusnavi" ]]; then
                    doins -r "modules/registry-arus" || die
                else
                    die
                fi
            fi
        done
    fi

    # ... optional supervisord config file
    if [ "x${DISABLE_SUPERVISORD_CONF}" == "x" ]; then
      insinto "${CONF_BASEDIR}"
      echo -e ${SUPERVISORD_FILE} > "${WORKDIR}/${REAL_PN}.conf"
      doins ${REAL_PN}.conf
    fi

    # ... and configuration files
    insinto "${CONF_DIR}"
    for c in $(ls conf); do
        if [ -d "conf/${c}" ]; then
            doins -r "conf/${c}" || die
        else
            doins "conf/${c}" || die
        fi
    done

    fowners -R otr:otr ${INSTALL_DIR}
}
